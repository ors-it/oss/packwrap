# -*- coding: utf-8 -*-

# Python
import logging
import pprint
import os

# Own
from packwrap import PackWrapSession
from packwrap.utils import full_path, symlink_dirs

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=2)


def pre_process(
        pack_wrap: PackWrapSession, conf: dict={}) -> bool:
    '''
    Link additional directories in the HTTP working dir
    :param tmp_dir:
    :param http_dir:
    :param cache_dir:
    :param conf:
    :return:
    '''
    http_d = pack_wrap.http_d

    for http_subd in conf['directories']:
        http_subd = pack_wrap.tpl_str(http_subd)
        symlink_dirs(
            full_path(http_subd),
            os.path.join(http_d, os.path.basename(http_subd))
        )

    return True
