# -*- coding: utf-8 -*-
'''
Put files in the Packer Cache
'''
# Python
import os

# Debug & Logging
import logging
import pprint

# Import own libs
from packwrap import PackWrapSession
from packwrap.utils import full_path, cache_key, fetch_file, symlink_dirs

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=2)


def pre_process(
        pack_wrap: PackWrapSession, conf: dict={}) -> bool:
    '''
    download something into the cache & link it somewhere
    '''

    for item in conf['cache']:
        source = pack_wrap.tpl_str(item['url'])
        cache = full_path(
            pack_wrap.cache_d,
            cache_key(source)
        )
        res = fetch_file(source, cache,
                         pack_wrap.tpl_str(item.get('checksum', None)),
                         pack_wrap.tpl_str(item.get('checksum_type', None)))

        links = item.get('links', [])
        if not isinstance(links, (list, tuple)):
            links = [links]

        for link in links:
            link = pack_wrap.tpl_str(link)
            if link.endswith(os.sep):
                link = os.path.join(link, os.path.basename(source))

            symlink_dirs(
                cache,
                link
            )

    return True
