#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''

Wrap around Packer

JSMinifiying
    allows JavaScript-style comments in the JSONs

Template merging
    _pw_include:
        path-to-template (no ext, relative paths work)
        optional :strat or [key:strat, strat, key:strat]

SSH pub/privkey generation
HTTP dir setup
   _pw_httpd: [path1,path2,path3]

'''
# Python
import json
import os
import tempfile
import importlib

# Logging & Debugging
import logging
import pprint

# Own
from packwrap import tpl
from packwrap.utils import run, full_path

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=2)

log.setLevel(logging.DEBUG)


def version(path=None):
    versions = {
        'PackWrap': '0.6-alpha',
        'Packer': run('packer version', capture=True)[0].splitlines()[0].split()[-1][1:].decode('utf8')
    }
    try:
        versions['templates'] = run('git rev-parse HEAD', cwd=path, capture=True)[0].splitlines()[0].decode('utf8')
    except:  # pylint: disable=bare-except
        pass

    return versions


def packer(
        command,
        args=[],
        user_vars={},
        var_files=[],
        templates=[],
        path=None,
):
    '''
    Process & exec Packer for given stuff

    :param command: Packer command
    :param args: list of Packer arguments
    :param var_files: list of variables json files
    :param user_vars: dict of extra vars
    :param templates: list of templates
    :param path: becomes current working directory
    :return: idunnolookbelow
    '''
    if not path:
        path = os.getcwd()
    else:
        path = full_path(path)
        # log.warn('PATH: {}'.format(path))
        os.chdir(path)

    if command not in ('build', 'inspect', 'validate'):
        run(['packer', command] + args, cwd=path)
        return

    pwsess = PackWrapSession(path, templates, var_files, user_vars)

    pwsess.run(command, args)

    pwsess.process('ppost-processors')


class PackWrapSession(object):
    PREFIX = '_pw_'

    __slots__ = ('tmpl', 'user', 'vars',
                 'http_d', 'cache_d', 'temp_d',
                 'path', 'tmpl_f', 'var_f')

    def __init__(self, path, templates, var_files, vars_user):
        # Create the temp work & HTTP dir
        tmp_d = tempfile.mkdtemp(prefix=self.PREFIX)
        self.temp_d = tmp_d

        http_d = os.path.join(tmp_d, 'http_d')
        os.mkdir(http_d)

        # Setup Packer env to benefit all
        os.environ.update({
            'PACKWRAP_DIR': tmp_d,
        })

        # Kick the template rendering engine into gear
        pack_tmpl, pack_vars = tpl.process(templates, var_files, vars_user, path=path)

        # Compile the list of user variables
        pack_user = pack_tmpl.get('variables', {})
        pack_user.update(pack_vars)

        self.tmpl = pack_tmpl
        self.user = pack_user
        self.vars = pack_vars

        self.path = path
        self.cache_d = full_path(os.environ.get('PACKER_CACHE_DIR', '~/.packer_cache'))
        self.http_d = http_d

        # Allow pre-processors to modify pack_wrap & tmpl & such before dumping
        if not self.process('pre-processors'):
            raise Exception('Failed to run through pre-processors')

        # Dump the resultant set of template and variables
        tmpl_f = os.path.join(tmp_d, 'tmpl.json')
        with open(tmpl_f, 'w') as _pjfh:
            json.dump(pack_tmpl, _pjfh, indent=3, sort_keys=True)
        self.tmpl_f = tmpl_f

        var_f = os.path.join(tmp_d, 'vars.json')
        with open(var_f, 'w') as _pvfh:
            json.dump(pack_vars, _pvfh, indent=3, sort_keys=True)
        self.var_f = var_f

    def process(self, group):
        '''
        Process a phase
        :param group: group (Python/JSON NS) to walk through
        :return: TODO
        '''
        log.info('Processing {}...'.format(group))
        coll = self.tmpl.get(self.PREFIX + group, [])
        group = group.replace('-', '_')

        for comp in coll:
            log.debug('Processing {}...'.format(comp['type']))
            # log.debug('comp: {}'.format(comp))
            comp = {k: self.tpl_str(v) for k, v in comp.items()}
            # log.debug('comp: {}'.format(comp))
            comp_mod = importlib.import_module('packwrap.{}.{}'.format(group, comp.pop('type')))
            res = getattr(comp_mod, group[:-3])(self, comp)
            if not res:
                raise Exception('Error processing {}'.format(comp))

        return True

    def tpl_str(self, go_str):
        '''
        Render a single string similar the way Packer does it
        :param go_str:
        :return:
        '''
        return tpl.go(go_str, self.tmpl, self.user)

    def run(self, command, args):
        return run(['packer', command, '-var-file=' + self.var_f] + args + [self.tmpl_f], cwd=self.path)

    def __del__(self):
        log.info('I would have purged {}'.format(self.temp_d))
