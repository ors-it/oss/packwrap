# -*- coding: utf-8 -*-
import logging
import pprint
import os

# Import own libs
from packwrap import PackWrapSession
from packwrap.utils import run

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=2)


def pre_process(
        pack_wrap: PackWrapSession, conf: dict={}) -> bool:
    """
    create an SSH key
    :param conf:
    :return:
    """
    temp_d = pack_wrap.temp_d
    http_d = pack_wrap.http_d

    key_t = conf.get('key_type', 'rsa')
    key = os.path.join(temp_d, 'ssh.' + key_t)

    # Create a key
    _, _, retcode = run('ssh-keygen -t {} -P "" -C "Packer Provisioning" -f {}'.format(key_t, key), capture=True)

    if retcode != 0:
        return False

    os.symlink(
        os.path.join(temp_d, 'ssh.{}.pub'.format(key_t)),
        os.path.join(http_d, 'ssh.{}.pub'.format(key_t))
    )

    return True
