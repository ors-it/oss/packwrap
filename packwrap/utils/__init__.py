# -*- coding: utf-8 -*-

# Python
import hashlib
import os
import subprocess
import sys
from subprocess import TimeoutExpired, SubprocessError
import requests

# Logging & Debug
import logging
import logging.handlers
import pprint

# To be filled by config
log = None
pp = pprint.PrettyPrinter(indent=2)


def set_logger():
    '''
    Initialize logging
    '''
    # First, we override the logger Class to include a TRACE level
    FORMAT = dict(
        LOG='{asctime} [{levelname}] {name}: {message}',
        DATE='%Y/%m/%d %H:%M:%S',
    )

    # Now let's setup the root logger
    logger = logging.getLogger(None)

    if os.environ.get('PACKER_LOG', False):
        # TODO: We might interpret PACKER_LOG as loglevel rather than bool
        logger.setLevel('DEBUG')
        if 'PACKER_LOG_PATH' in os.environ:
            # if 'PACKER_LOG_PATH' == 'output_dir':
            #     pass # Cant do this before tpl rendering, but need logging before that :(
            #          # Janitor?
            # else:
            log_path = full_path(os.environ['PACKER_LOG_PATH'])

            handler = logging.handlers.FileHandler(log_path)
            handler.setFormatter(logging.Formatter(FORMAT['LOG'], datefmt=FORMAT['DATE'], style='{'))
            logger.addHandler(handler)

    handler = logging.StreamHandler(stream=sys.stderr)
    handler.setFormatter(logging.Formatter(FORMAT['LOG'], datefmt=FORMAT['DATE'], style='{'))
    logger.addHandler(handler)

    # Now create a sub-logger for usage within this module
    global log
    log = logging.getLogger('packwrap')
    # log.setLevel(level)


def run(cmd, cwd=None, capture=False, env={}):
    '''
    Simple but effective wrapper around subprocess.popen
    :param cmd: string or list
    :param cwd: Working Directory
    :param capture: True/False to capture & return stdout, stderr, returncode
    :param env: Additional env vars
    :return: stdout, stderr, returncode
    '''
    # cmd = shlex.split(cmd)
    if isinstance(cmd, (list, tuple)):
        cmd = ' '.join(cmd)

    cwd = cwd or os.getcwd()

    log.debug('Executing {} in {}...'.format(cmd, cwd))

    cenv = os.environ.copy()
    cenv.update(env)
    kwargs = {
        'cwd': cwd,
        'shell': True,
        'env': cenv
    }

    if capture:
        kwargs.update({
            'stdout': subprocess.PIPE,
            'stderr': subprocess.PIPE
        })

    runner = subprocess.Popen(cmd, **kwargs)

    try:
        stdout, stderr = runner.communicate(timeout=3600)
        returncode = runner.returncode
    except (TimeoutExpired, SubprocessError):
        runner.kill()
        stdout, stderr = runner.communicate()
        returncode = runner.returncode
    except Exception as e:
        # I dunno
        raise e

    if returncode != 0:
        log.warn('{}: Failed execution with {}'.format(cmd, stderr or returncode))

    return stdout, stderr, returncode


def fetch_file(url, output, checksum=None, checksum_algo=None):
    chunk_size = 256

    if os.path.isfile(output):
        if checksum and checksum_algo and checksum_file(output, checksum_algo, checksum):
            log.debug('{} is already there'.format(url))
            return True
        os.remove(output)

    log.debug('Fetching {}...'.format(url))
    fetch = requests.get(url, stream=True)
    if fetch.status_code != 200:
        return False

    # Try to find a content-length
    len_headers = [header for header in fetch.headers.keys() if header.endswith('content-length')]
    if not len_headers:
        size = 1
    else:
        size = int(fetch.headers[len_headers[0]])

    done = 0
    perc = 0
    with open(output, 'wb') as f:
        for chunk in fetch.iter_content(chunk_size):
            f.write(chunk)
            if checksum_algo:
                checksum_algo.update(chunk)

            done += len(chunk)
            nperc = round(done / size, 2)
            if nperc > perc:
                perc = nperc
                sys.stdout.write('\rDownloading {}: {}%'.format(url, int(nperc * 100)))
                sys.stdout.flush()


def fetch_contents(url):
    r = requests.get(url)
    if r.status_code == 200:
        return r.raw
        # with open(output, 'wb') as f:
        #     for chunk in r.iter_content(1024):
        #         f.write(chunk)


def checksum_file(file, algo, validate=None):
    '''
    Checksum a file
    :param file: file to checksum
    :param algo: e.g. 'sha256'
    :param validate: if this is set, return True/False if checksum matches
    :return: False/True with validation or checksum
    '''
    algo = hashlib.new(algo)
    chunk_size = 256

    with open(file, 'rb') as f:
        chunk = f.read(chunk_size)
        while len(chunk) > 0:
            algo.update(chunk)
            chunk = f.read(chunk_size)

    res = algo.hexdigest()
    if validate:
        return res == validate
    else:
        return res


def cache_key(url):
    '''
    Packer-compatible cache file name
    See:
        packer/common/step_download.go
        packer/cache.go

    :param url: path of source
    :return: cschefile basename
    '''
    ext = url.split('.')[-1]
    c_key = hashlib.sha1(url.encode('utf-8')).hexdigest() + '.' + ext
    c_key = hashlib.sha256(c_key.encode('utf-8')).hexdigest() + '.' + ext

    return c_key


def cache_has(cache, url, checksum=None, checksum_type=None):
    '''
    Check if a URL is already in the cache
    :param url:
    :param checksum:
    :param checksum_type:
    :return:
    '''
    cache_file = os.path.join(cache, cache_key(url))
    if not os.path.isfile(cache_file):
        return False
    elif checksum is not None and checksum_type is not None:
        return checksum_file(cache_file, checksum_type, checksum)

    # I'm likely lying here?
    return True


def full_path(*elements):
    '''
    Path maker
    TODO: Packer don't do no expandvars, should we?
    :param elements (list of) elements
    :return:
    '''
    # if not isinstance(elements, (list, tuple)):
    #     elements = [elements]

    return os.path.abspath(
        os.path.expanduser(
            os.path.expandvars(
                os.path.join(os.getcwd(), *elements))))


def symlink_dirs(src, dst):
    '''
    Create a symlink and all the upper dirs
    :param src:
    :param dst:
    :return:
    '''

    log.warn('SYMLINKING {} TO {}'.format(src, dst))
    dst_dir = os.path.dirname(dst)
    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir, exist_ok=True)

    os.symlink(
        src,
        dst
    )


def pwgen(len=32, pron=True):
    '''
    Generate a password of len.
    Needs apg in PATH
    :param len: length of password
    :param pron: Pronounceable
    :return: should-be-shell-safe string
    '''

    algo = int(not pron)

    #  -E "\"\\'\$\`}{"
    stdin, stderr, exitc = run(
        'apg -n 1 -a {0} -M SNCL -m {1} -x {1} -E "\\"\\\\\'\\$\\`}}{{"'.format(algo, len),
        capture=True
    )

    if exitc:
        raise Exception(stderr)

    return stdin[:-1].decode('utf8')
