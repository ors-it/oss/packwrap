# -*- coding: utf-8 -*-
'''
Dump env vars in http_dir/env

example:

{
 "type": "var_file",
 "format": "env|json",
 "file_name": "packwrap.env|json",
 "environment_vars": [
   "PACKWRAP_PW",
   "CI_BUILD_TOKEN",
   "BUILD_ID",
  ],
  "user_vars": [
    "osrelease",
    "checksum_type",
   ]
  "variables": [
    ["MYVAR1", "{{ user `myvar1` }}"]
  ]
}
'''
# Python
import json
import os
from collections import OrderedDict

# Debug & Logging
import logging
import pprint

# Import own libs
from packwrap import PackWrapSession

log = logging.getLogger(__name__)
ppr = pprint.PrettyPrinter(indent=2).pprint


def pre_process(
        pack_wrap: PackWrapSession, conf: dict={}) -> bool:
    '''
    Generate the pass & put it in env
    '''

    vars = OrderedDict()

    for param, lookup in (
            ('environment_vars', os.environ),
            ('user_vars', pack_wrap.user)):
        for var in conf.get(param, []):
            try:
                vars[var] = lookup[var]
            except KeyError:
                pass

    for var, val in conf.get('variables', []):
        vars[var] = pack_wrap.tpl_str(val)

    fmt = conf.get('format', 'env')
    if 'file_name' in conf:
        target = pack_wrap.tpl_str(conf['file_name'])
    else:
        target = 'packwrap.' + fmt

    target = os.path.join(pack_wrap.http_d, target)

    with open(target, 'w') as _vfh:
        if fmt == 'json':
            json.dump(vars, _vfh, indent=3)
        elif fmt == 'env':
            _vfh.writelines(['{}="{}"\n'.format(k, v) for k, v in vars.items()])
        else:
            raise ValueError('Wrong fmt {}'.format(fmt))

    return True
