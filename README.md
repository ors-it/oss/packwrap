# PackWrap
Your Packer buddy.

Stay as true to Packer syntax as possible while adding quite a few handy-dandy utilz.

# Install
`pip install packwrap`

# Use

packwrap quite literally wraps around Packer and it's syntax. It is therefore identical to Packers' commandline syntax, passing on most arguments.
However, the `build`, `inspect` and `validate` commands are overloaded with the following additions:

* `-path`: Set the active directory ('.') for path resolution
* `-var-file`: Can now be repeated to combine multiple files together
* The same goes for the templates

## Example

```sh
packwrap build -path ./FreeBSD -var-file=11.0.json -var-file=../common/cloudcreds.json main.json to-s3.json
```

# The working directory
PackWrap will create a temporary working directory to do it's stuff; of especial interest for templating is the HTTP dir.

## Example usage
```json
  "variables": {
    "packwrap_dir": "{{env `PACKWRAP_DIR`}}"
  },

  "builders": [
    { 
      "http_directory": "{{user `packwrap_dir`}}/http_d/",
```

# Features
## Templates & Variable files
### Comments
JavaScript style comments are supported in the JSON files now.

```json
{
  "builders": {  
    "type": "qemu", // Look ma
    "accelator": "kvm"
    /*
    I'm 
    a
    comment
    */
  }
}
```

### Include other JSONs

PackWrap supports an `include` directive.  The merger will take place in-memory before `pre-processors`, after which new JSONs will be dumped in `$PACKWRAP_DIR` and `packer` will be launched:
```json
{
  "_pw_include": [{ 
    "template": "../common/base.json", //Relative to the template
    "strategy": "smart" //Default strategy for merging keys and lists
    "strategies": [
      ["post-processors", "append"] //see below 
    ]
  }]
}
```

There are various strategies:
* `replace`: completely overwrite the key
* `reverse`: include tmpl in bag instead of the other way around
* `update`: update each item in position
* `default`: extend for lists, update for dicts
* `smart`: `default` except `builders` to `update`

`smart` is the global default

## pre-processors

Before Packer starts, do stuff. Syntax & workings are similar to Packer's own phases.

### http_directories 

Link additional subdirectories to the HTTP root

```json
{
  "_pw_post-processors": [{ 
    "type":"http_directories",
    "directories": [
      "./install" //Relative to the set path, becomes 'install' subdir on HTTP server
    ]
  }] 
}
```

### ssh_key

* Needs `ssh-keygen` in PATH to work
* public key will be made available in HTTP root as `ssh.{{key_type}}.pub`

```json
{
  "_pw_pre-processors": [{ 
    "type": "ssh_key",
    "key_type": "rsa" //rsa is default, fed directly into ssh-keygen
  }],
  "builders": [{ 
    "ssh_private_key_file": "{{user `packwrap_dir`}}/ssh.rsa"
  }]
}
```

### cacher

Download items and put them in the Packer cache the way Packer would.

```json
  "_pw_pre-processors": [
    { "type": "cacher",
      "cache": [{
        "url": "https://{{user `channel`}}.release.core-os.net/amd64-usr/{{user `version`}}/coreos_production_image.bin.bz2",
        "checksum_type": "{{user `checksum_type`}}",
        "checksum": "{{user `bin_checksum`}}",
        "links": "{{user `http_dir`}}/{{user `version`}}/"
      }
    ]}
```

## ppost-processors

post Packer stuff

### CloudStack

Upload template to CS 
TODO

### Aurora

Upload template to PCextreme Aurora regions
TODO

### janitor

```json
{
  "_pw_ppost-processors": [{
    "type": "janitor",
    "builds": 3, //number of builds to keep
    "cache_days": 90 //everything older than this will be removed from the cache
  }]
}
```

# Extending PackWrap
Analogous to Packer a PackWrapSession object is created and passed on.
```python
class PackWrapSession(object):
    PREFIX = '_pw_'

    __slots__ = (
        'tmpl',    # fully merged template as dict (before dumping to disk for pre-processors)
        'user',    # fully rendered variables dict (PackWrap performs some fake-rendering of {{ }} syntaxes in the relative sections) 
        'vars',    # fully merged variables as dict (before dumping to disk for pre-processors)
        'http_d',  # New HTTP root 
        'cache_d', # Packer's cache dir
        'temp_d',  # PackWrap temporary directory
        'path',    # active template directory
        'tmpl_f',  # filename of rendered template
        'var_f'    # filename of rendered variables file
    )

    def tpl_str(self, go_str):
        '''
        Render a single string similar the way Packer does it
        '''
```

Consider the following pseudo-code:
```python
from packwrap import PackWrapSession

import packwrap.post_processor
import packwrap.pre_processor

plugin_name = 'example'

pack_wrap = PackWrapSession()

# conf = dict of configuration sans 'type'
packwrap.ppost_processor.example.ppost_process(pack_wrap, conf)
packwrap.pre_processor.example.pre_process(pack_wrap, conf)
```
