# -*- coding: utf-8 -*-

# Python
import os

# Logging & Debugging
import logging
import pprint

# Ext
import demjson

# Own
from packwrap.utils import full_path

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=2)

# YUGH. Is already in PackWrapSession
PREFIX = '_pw_'


def render(tpldir, tpl_incs, bag, parent_conf={}):
    '''
    render a template
    and merge it into the bag

    :param tpldir: cwd for templates
    :param tpl_incs: [{
             template: filename (cwd=template dir),
             strategy: default strategy
             strategies: [
               [key, strat]
             ]
          }, etc...]
          strats:
            repl: replace key in bag with key in tpl
            append: append items to lists rather than overwriting them
            rev: reverse the update (bag overwrites tpl rather than other way round)
    :param bag: passed on dict
    :return:
    '''
    log.debug('Parsing {} in {}'.format(tpl_incs, tpldir))

    for tpl_inc in tpl_incs:
        tpl_f = full_path(tpldir, tpl_inc.pop('template'))
        parent_conf.update(tpl_inc)
        tpl_inc = parent_conf
        strat = tpl_inc.get('strategy', 'smart')
        strat_map = {}
        if strat == 'smart':
            strat_map.update({
                'builders': 'update'
            })
            strat = 'default'
        strat_map.update(tpl_inc.get('strategies', []))
        log.debug('Importing {} with merge strategy {} or {}...'.format(tpl_f, strat, strat_map))
        # with open(tpl, 'r') as _fh:
        #     tpl = json.load(_fh)
        # Allow comments in JSON instead
        tpl = demjson.decode_file(tpl_f, strict=True, allow_comments=True)

        sub_tpl = tpl.pop(PREFIX + 'include', [])
        if sub_tpl:
            render(os.path.dirname(tpl_f), sub_tpl, bag, tpl_inc.copy())

        assert isinstance(tpl, dict), 'Unable to render template {}'.format(tpl_f)

        # log.debug('Starting import of tpl {} into bag {}'.format(tpl, bag))
        for k in tpl.keys():
            k_strat = strat_map.get(k, strat)
            log.debug('Merging {} with strategy {}'.format(k, strat))

            if k not in bag or k_strat == 'replace':
                # pp.pprint('Overwriting {}'.format(k))
                bag[k] = tpl[k]
                continue
            elif isinstance(tpl[k], dict):
                if k_strat == 'reverse':
                    tpl[k].update(bag[k])
                    bag[k] = tpl[k]
                elif k_strat == 'default':
                    bag[k].update(tpl[k])
            elif isinstance(tpl[k], list):
                if k_strat == 'default':
                    bag[k].extend(tpl[k])
                elif k_strat == 'update':
                    for idx, data in enumerate(tpl[k]):
                        bag[k][idx].update(data)
            else:
                # pp.pprint('Strange')
                bag[k] = tpl[k]

    # pp.pprint(bag)
    return True


def go(tplstr, tplvars={}, user={}):
    '''
    Fake the shit out of tpl vars
    :param tplstr: the string to parse in the style of Packer
    :param tplvars: legit variables
    :param user: legit user variables
    :return: Packer-like rendered string
    '''
    # log.trace('Parsing {}'.format(tplstr))
    if not isinstance(tplstr, (bytes, str)) or '{{' not in tplstr:
        return tplstr

    res = ''
    while tplstr:
        flat, _, tplstr = tplstr.partition('{{')
        res += flat
        var, _, tplstr = tplstr.partition('}}')
        var = var.strip()
        if not var:
            continue
        elif '`' in var:
            macro, _, var = var.partition('`')
            macro = macro.strip()
            var = var.strip(' `')
            res += {
                'user': user,
                'env': os.environ,
            }[macro][var]
        else:
            log.error('Resolving {} is currently broken'.format(var))

            var = var.lstrip('.')
            var_map = {
                'HTTPIP': 'BROKEN',
                'HTTPPort': 'BROKEN',
                'Vars': 'BROKEN',
                'Path': 'BROKEN',
                'Name': 'BROKEN',
                'timestamp': 'BROKEN'
            }

            res += [var_map[var]]

    # Recursive in case of embedded vars in vars
    return go(res, tplvars, user)


def process(templates, var_files, x_vars={}, path=None):
    '''
    :param templates: list of template paths
    :param var_files: list of var files
    :param x_vars: dict of extra vars
    '''
    path = path or os.getcwd()

    assert isinstance(templates, (list, tuple))
    assert isinstance(var_files, (list, tuple))
    assert isinstance(x_vars, dict)

    res_tmpl = {}
    res_vars = {}
    # Load the template & variables
    for src, dst in [(templates, res_tmpl), (var_files, res_vars)]:
        if not isinstance(src, (list, tuple)):
            src = [src]

        conf = [{'template': s_src} for s_src in src]
        # log.info('Processing {} in {}'.format(conf, os.getcwd()))

        if not render(path, conf, dst):
            raise ImportError

    res_vars.update(x_vars)

    return res_tmpl, res_vars
