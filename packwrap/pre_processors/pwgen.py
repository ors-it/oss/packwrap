# -*- coding: utf-8 -*-
'''
Generate a password

Must have apg in PATH

example:

{
 "type": "pwgen",
  "length": 32,
  "environment_var": ENV var name
}
'''
# Python
import os

# Debug & Logging
import logging
import pprint

# Import own libs
from packwrap import PackWrapSession
from packwrap.utils import pwgen

log = logging.getLogger(__name__)
ppr = pprint.PrettyPrinter(indent=2).pprint


def pre_process(
        _: PackWrapSession, conf: dict={}) -> bool:
    '''
    Generate the pass & put it in env
    '''

    pw = pwgen(conf.get('length', 32))

    os.environ[conf.get('environment_var', 'PACKWRAP_PW')] = pw

    return True
